//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ], 
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: 'Quantas questões voce respondeu?',
        respostas: [
            { valor: 3, texto: '5' },
            { valor: 0, texto: '1' },
            { valor: 1, texto: '6' },
            { valor: 2, texto: '3' }
        ]   
    }
]

var numeroDaPergunta = -1;
var pontuacao = 0;
var porcentagemDaPontuacao;

function mostrarQuestao() {
    
    var respondeu = false;
    var itemMarcado = 0;
    itens = document.getElementsByName("resposta");//Armazenando em itens os radio buttons

    //Verifica se algum radio button foi selecionado
    for(var i = 0; i < 4; i++){
        if(itens[i].checked){
            respondeu = true;
            itemMarcado = i;
            calcularPontuacao(itemMarcado,numeroDaPergunta);
            itens[i].checked = false;
        }
    }
    
    //Permissão para responder próxima pergunta
    if(respondeu || numeroDaPergunta == -1){
        
        numeroDaPergunta++;
        if(numeroDaPergunta >= 0 && numeroDaPergunta <=5){
            //Mostrando a pergunta
            document.getElementById("titulo").innerHTML=perguntas[numeroDaPergunta].texto;
        
            //Spans dos radio buttons
            textoDosItens = document.getElementsByTagName("span");
        
            //Mostrando os itens da pergunta
            for(var i = 0; i < 4; i++){
                textoDosItens[i].innerHTML = perguntas[numeroDaPergunta].respostas[i].texto;    
            }

            //Trocando o texto do botão
            document.getElementById("confirmar").innerHTML = "Próxima";

            //Permitindo que o HTML do quiz apareça
            document.getElementById("listaRespostas").style.display="initial";
        }
        
    
    }

    if(numeroDaPergunta == 6){
        finalizarQuiz();
    }

}

function calcularPontuacao(itemMarcado,numeroDaPergunta){
    //Receber o valor do item marcado
    valor = perguntas[numeroDaPergunta].respostas[itemMarcado].valor;
    
    //Somatório para obter pontuação total
    pontuacao = pontuacao + valor;

    //Calculo pontuação percentual
    porcentagemDaPontuacao = (pontuacao/18)*100;
}

function finalizarQuiz() {
    //Esconde o html das perguntas
    document.getElementById("listaRespostas").style.display="none";
    
    document.getElementById("titulo").innerHTML="Resultado";

    //Mostra resultado
    document.getElementById("resultado").innerHTML="Sua pontuação foi " + porcentagemDaPontuacao.toFixed(2) + "%";
    
    //Esconde o botão
    document.getElementById("confirmar").style.display="none"; 
}